package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium03;

import java.math.BigInteger;
import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args){

        Scanner in = new Scanner(System.in);

        System.out.println("Podaj n: ");
        int n = in.nextInt();
        BigInteger a = new BigInteger("2");
        a = a.pow(n);


        System.out.println(a.toString());
    }
}
