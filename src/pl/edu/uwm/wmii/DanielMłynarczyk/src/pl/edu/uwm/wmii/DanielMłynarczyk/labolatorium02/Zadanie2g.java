package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium02;

import java.util.Random;
import java.util.Scanner;
public class Zadanie2g {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi by� z przedzia�u [1;100]!");
            System.exit(1);
        }
        int[] tab = new int[n];
        generuj(tab,n,-999,999);
        wypisz(tab);
        odwrocFragment(tab,2,5);
        wypisz(tab);



    }


    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random generuj = new Random();
        for(int i=0; i<n; i++){
            tab[i] = generuj.nextInt(maxWartosc-minWartosc+1)+minWartosc;
        }
    }

    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
    public static void odwrocFragment(int[] tab, int lewy, int prawy){
            int tmp;
            for(int i=lewy; i<prawy; i++){
                tmp = tab[i];
                tab[i] = tab[prawy-i-1];
                tab[prawy-i-1] = tmp;
        }
    }
}
