package kolokwium2;

public class Autobus extends SrodekTransportu{

    public Autobus(int iloscMiejsc) {
        super();
        this.iloscMiejsc = iloscMiejsc;
        obliczCene();
    }

    @Override
    public String toString() {
        return "Autobus: ilosc miejsc: " + this.iloscMiejsc +
                ", cena biletu: " + getCenaBiletu();
    }
}
