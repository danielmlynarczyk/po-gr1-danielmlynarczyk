package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium03;

import java.util.Scanner;

public class Zadanie1f {
    public static void main(String[] args){
        Scanner scanner = new Scanner((System.in));
        System.out.println("Podaj napis: ");
        String napis = scanner.nextLine();
        System.out.println(napis + ": " + change(napis));

    }
    public static String change(String str){
        StringBuffer string = new StringBuffer("");
        for(int i = 0; i < str.length(); i++){
            char c = str.charAt(i);
            if(Character.isUpperCase(c)){
                string.append(Character.toLowerCase(c));
            }
            else{
                string.append(Character.toUpperCase(c));
            }
        }
        return string.toString();
    }
}
