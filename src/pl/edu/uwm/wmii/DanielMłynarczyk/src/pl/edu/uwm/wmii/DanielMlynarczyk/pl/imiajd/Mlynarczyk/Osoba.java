package pl.imiajd.Mlynarczyk;

import java.time.LocalDate;
import java.util.Objects;

public class Osoba  implements Cloneable, Comparable<Osoba>{
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    public int compareTo(Osoba o) {
        int res = nazwisko.compareTo(o.nazwisko);
        if(res == 0) res = dataUrodzenia.compareTo(o.dataUrodzenia);
        return res;

    }

    @Override
    public String toString() {
        return "Nazwa klasy: " + getClass() + "\n" + "[" +
                "Nazwisko: " + getNazwisko() + "]" + "\n" + "[" +
                "Data urodzenia: " + getDataUrodzenia() + "]";

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba osoba = (Osoba) o;
        return Objects.equals(nazwisko, osoba.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba.dataUrodzenia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwisko, dataUrodzenia);
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }
}
