package pl.imiajd.Mlynarczyk;

public class TestOsoba {
    public static void main(String[] args){
        Osoba osoba = new Osoba("Mlynarczyk", 2000);
        System.out.println(osoba.getRokUrodzenia());
        System.out.println( osoba.getNazwisko());
        System.out.println(osoba.toString());
        Osoba student = new Student("Kowalski", 1989, "Informatyka");
        System.out.println("------------------");
        System.out.println();
        System.out.println(student.getNazwisko());
        System.out.println(student.getRokUrodzenia());
        System.out.println(student.toString());
        Osoba nauczyciel = new Nauczyciel("Popowski", 1977, 5400);
        System.out.println("------------");
        System.out.println();
        System.out.println(nauczyciel.getRokUrodzenia());
        System.out.println(nauczyciel.getNazwisko());
        System.out.println(nauczyciel.toString());
    }
}
