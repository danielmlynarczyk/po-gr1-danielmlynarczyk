package pl.imiajd.Mlynarczyk;

public class Adres {
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private int kod_pocztowy;

    public Adres(int numer_mieszkania){
        this.numer_mieszkania = numer_mieszkania;
    }
    public Adres(String ulica, int numer_domu, String miasto, int kod_pocztowy){
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }
    public void show(){
        System.out.println("Kod pocztowy: " + kod_pocztowy + "Miasto: " + miasto);
        System.out.println("Ulica: " + ulica + "Numer domu: " + numer_domu);
    }

}
