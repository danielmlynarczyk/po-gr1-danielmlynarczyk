package pl.imiajd.Mlynarczyk;

import java.awt.*;

public class BetterRectangle extends Rectangle {

    public BetterRectangle(int x, int y, int width, int height){
        super(x, y, width, height);
        this.setLocation(x,y);
        this.setSize(width,height);
    }

    public int getPremeter(){
        return 2 * width + 2 * height;
    }
    public int getArea(){
        return width * height;
    }


}
