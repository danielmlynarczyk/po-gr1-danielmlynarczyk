package labolatorium07;

import pl.imiajd.Mlynarczyk.Flet;
import pl.imiajd.Mlynarczyk.Fortepian;
import pl.imiajd.Mlynarczyk.Instrument;
import pl.imiajd.Mlynarczyk.Skrzypce;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {

        ArrayList<Instrument> orkiestra = new ArrayList<>();
       orkiestra.add(new Fortepian("Fortepian", LocalDate.of(2020,12,12)));
       orkiestra.add(new Skrzypce("Skrzypce", LocalDate.of(2020,11,6)));
       orkiestra.add(new Flet("Flet", LocalDate.of(2020,1,2)));


        for (Instrument ins: orkiestra
             ) {
            ins.dzwiek();
            System.out.println("--------------");
            System.out.println(ins.toString());
            System.out.println("--------------");
        }


    }
}
