package labolatorium08;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.CollationElementIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Zad3 {
    public static void main(String[] args) throws FileNotFoundException {

        try {
            System.out.println("Podaj nazwe pliku: ");
            ArrayList<String> lines = new ArrayList<>();
            Scanner scanner = new Scanner(System.in);
            File file =  new File(scanner.nextLine());

            scanner = new Scanner(file);

            while(scanner.hasNextLine()) {
                String line = scanner.nextLine();
                lines.add(line);
            }
            scanner.close();

            for(String lin: lines) {
                System.out.println(lin);
            }
            Collections.sort(lines);
            System.out.println(" ");

            for(String lin: lines) {
                System.out.println(lin);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }
}
