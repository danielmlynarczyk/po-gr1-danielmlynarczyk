package pl.imiajd.Mlynarczyk;

import java.time.LocalDate;

 public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, double pobory, String imiona, LocalDate dataUrodzenia, boolean plec, LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return "Nazwisko: " + getNazwisko() + "\n" +
                "Imie: " + getImiona() + "\n" +
                "Data urodzenia: " + getDataUrodzenia() + "\n" +
                "Plec: " + getPlec() + "\n" +
                "Data zatrudnienia: " + getDataZatrudnienia() + "\n" +
                "Zarobki: " + getPobory();
    }
    
    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}