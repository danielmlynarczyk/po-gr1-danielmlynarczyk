package kolokwium2;

public interface IZarzadzaj {
    void DodajAutobus(int iloscMiejsc);
    void DodajPociag(int iloscMiejsc, int dlugoscTrasy);
    void UsunOstatni();
    void Wyczysc();
}
