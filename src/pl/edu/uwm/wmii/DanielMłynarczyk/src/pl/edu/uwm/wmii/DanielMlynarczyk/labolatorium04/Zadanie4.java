package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium04;

import java.util.ArrayList;

public class Zadanie4 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> c = reversed(a);
        System.out.println(a);
        System.out.println(c);

    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> c = new ArrayList<>();

        for (int i = a.size()-1; i >= 0; i--) {
            c.add(a.get(i));
        }
        return c;
    }
}
