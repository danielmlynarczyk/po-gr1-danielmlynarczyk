package labolatorium10;

import java.util.Collections;
import java.util.LinkedList;

public class Zadanie3 {
    public static void main(String[] args) {
        LinkedList<String> lista = new LinkedList<>();
        lista.add("ala");
        lista.add("kasia");
        lista.add("ola");
        odwroc(lista);
        System.out.println(lista);

    }
    public static void odwroc(LinkedList<String> lista) {
        Collections.reverse(lista);
    }
}
