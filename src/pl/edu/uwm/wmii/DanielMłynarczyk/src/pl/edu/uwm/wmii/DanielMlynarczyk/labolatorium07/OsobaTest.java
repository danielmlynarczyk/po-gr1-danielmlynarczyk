package labolatorium07;
import java.time.LocalDate;
import pl.imiajd.Mlynarczyk.Osoba;
import pl.imiajd.Mlynarczyk.Pracownik;
import pl.imiajd.Mlynarczyk.Student;

public class OsobaTest
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[3];
        ludzie[0] = new Pracownik("Kowalski",15000,"Jan", LocalDate.of(1999,1,1),true,LocalDate.of(2020,9,12));
        ludzie[1] = new Pracownik("Mickiewicz",15000,"Monika", LocalDate.of(1989,4,5),false,LocalDate.of(2020,2,11));
        ludzie[2] = new Student("Mlynarczyk", "Informatyka", "Daniel", LocalDate.of(2000,12,31), true, 3.5);
        for (Osoba p: ludzie
             ) {
            System.out.println(p.getOpis());
            System.out.println("");
            System.out.println("------------");

        }
    }


}