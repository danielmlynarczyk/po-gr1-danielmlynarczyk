package pl.edu.uwm.wmii.DanielM�ynarczyk.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2e {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi by� z przedzia�u [1;100]!");
            System.exit(1);
        }
        int[] tab = new int[n];
        generuj(tab,n,-999,999);
        wypisz(tab);
        System.out.println("Dlugosc maksymalnego ciagu liczb dodatnich: " + dlugoscMaksymalnegoCiaguDodatnich(tab));

    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {
        Random generuj = new Random();
        for (int i = 0; i < n; i++) {
            tab[i] = generuj.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab){
        int licznik = 0;
        int dlugosc = 0;

        for(int i=0; i<tab.length; i++){
            if(tab[i]>0){
                licznik++;
            }
            else if(licznik>dlugosc){
                dlugosc = licznik;
                licznik = 0;
            }
            else {
                licznik = 0;
            }
            if(licznik>dlugosc){
                dlugosc = licznik;
            }
        }
        return dlugosc;
    }
}

