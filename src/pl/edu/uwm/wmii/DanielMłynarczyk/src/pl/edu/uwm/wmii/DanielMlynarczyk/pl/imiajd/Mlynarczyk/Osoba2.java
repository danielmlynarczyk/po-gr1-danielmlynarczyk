package pl.imiajd.Mlynarczyk;

import java.time.LocalDate;
import java.util.Objects;

public class Osoba2 implements Cloneable, Comparable<Osoba2>{
    private String nazwisko;
    private LocalDate dataUrodzenia;

    public Osoba2(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba2 osoba2 = (Osoba2) o;
        return Objects.equals(nazwisko, osoba2.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba2.dataUrodzenia);
    }

    @Override
    public String toString() {
        return "Nazwa klasy: " + getClass() +
                "[" + "Nazwisko: " + getNazwisko() + "]" +
                "[" + "Data urodzenia: " + getDataUrodzenia() +"]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwisko, dataUrodzenia);
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int compareTo(Osoba2 o) {
        return this.nazwisko.compareTo(o.nazwisko);
    }
}
