package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium03;

import java.math.BigDecimal;
import java.util.Scanner;

public class Zadanie5 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj n: (lata)");
        int n = scanner.nextInt();
        System.out.println("Podaj k: (kapital poczatkowy)");
        double k = scanner.nextDouble();
        System.out.println("Podaj p: (stope procentowa)");
        int p = scanner.nextInt();
        int w = (int) (k * (1 +(p/100 * 12)));
        double wynik = Math.pow(w,n*12);
        System.out.println(wynik);

    }

}
