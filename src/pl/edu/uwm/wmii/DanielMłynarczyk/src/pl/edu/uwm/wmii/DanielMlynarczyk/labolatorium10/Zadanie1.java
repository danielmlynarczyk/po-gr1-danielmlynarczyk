package labolatorium10;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>();
        int n = 2;
        pracownicy.add("Adam");
        pracownicy.add("Marek");
        pracownicy.add("Jan");
        pracownicy.add("Kuba");
        pracownicy.add("Daniel");
        System.out.println("Lista pracownikow: " + pracownicy);
        redukuj(pracownicy,n);
        System.out.println("Lista po redukcji: " + pracownicy);


    }

    public static void redukuj(LinkedList<String> pracownicy, int n) {
        ListIterator iterator = pracownicy.listIterator(n);
        while(iterator.hasNext()) {
            iterator.next();
            iterator.remove();
        }
    }
}
