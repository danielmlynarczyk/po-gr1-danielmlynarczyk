package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium04;

import java.util.ArrayList;

public class Zadanie5 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        System.out.println(a);
        reverse(a);
        System.out.println(a);

    }
    public static void reverse(ArrayList<Integer> a){

        for (int i = 0; i < a.size() / 2; i++) {
            int temp = a.get(i);
            a.set(i, a.get(a.size() - i - 1));
            a.set(a.size() - i - 1, temp);
        }
    }
}
