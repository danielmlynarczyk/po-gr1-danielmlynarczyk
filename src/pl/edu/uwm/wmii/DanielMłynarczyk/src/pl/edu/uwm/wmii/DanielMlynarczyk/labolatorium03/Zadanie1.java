package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium03;

import java.util.Scanner;

class Zadanie1 {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("------------PODPUNKT A------------");
        System.out.println("Podaj napis: ");
        String napis = scanner.nextLine();
        System.out.println(("Litera 'c' wystepuje: ")+countChar(napis,'c') + " razy");
        System.out.println("");
    }

    public static int countChar(String str, char c){
        int ile = 0;
        char znak;
        for(int i=0; i<str.length(); i++){
            znak = str.charAt(i);
            if(znak == c ){
                ile++;
            }
        }
        return ile;
    }
}