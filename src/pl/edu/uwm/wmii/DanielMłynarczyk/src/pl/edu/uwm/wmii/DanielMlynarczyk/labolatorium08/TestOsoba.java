package labolatorium08;
import pl.imiajd.Mlynarczyk.Osoba;

import java.time.LocalDate;
import java.util.*;

public class TestOsoba {
    public static void main(String[] args) {
        List<Osoba> osoba = new ArrayList<>();
        osoba.add (new Osoba("Mlynarczyk", LocalDate.of(2000,12,12)));
        osoba.add (new Osoba("Adamski", LocalDate.of(2000,12,12)));
        osoba.add (new Osoba("Kowalski", LocalDate.of(2000,5,22)));
        osoba.add (new Osoba("Kwiat", LocalDate.of(1998,8,30)));
        osoba.add (new Osoba("Kwiat", LocalDate.of(1979,7,22)));


        Collections.sort(osoba);
        for (Osoba os: osoba) {
            System.out.println(os);
            System.out.println("\n");
        }
    }
}