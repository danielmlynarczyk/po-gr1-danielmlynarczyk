package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium03;

public class Zadanie1b {
    public static void main(String[] args) {
        System.out.println(" :: " + countSubStr("programowanie", "prog"));

    }

    public static int countSubStr(String str, String subStr) {
        int atIndex = 0;
        int count = 0;

        while (atIndex != -1) {
            atIndex = str.indexOf(subStr, atIndex);

            if (atIndex != -1) {
                count++;
                atIndex += subStr.length();
            }
        }
        return count;
    }
}
