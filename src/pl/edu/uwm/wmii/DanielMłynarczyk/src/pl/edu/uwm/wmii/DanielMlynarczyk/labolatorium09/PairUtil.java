package labolatorium09;


public class PairUtil{
    public static <T> Pair<T> swap(Pair<T> o) {
        return new Pair<T>(o.getSecond(), o.getFirst());
    }
}