package labolatorium08;

import pl.imiajd.Mlynarczyk.Student;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestStudent {
    public static void main(String[] args) {
        List<Student> studenci = new ArrayList<>();
        studenci.add(new Student("Arciszewski", LocalDate.of(2006,12,11), 2.12));
        studenci.add(new Student("Bobek", LocalDate.of(1999,1,1), 3.5));
        studenci.add(new Student("Arciszewski", LocalDate.of(1976,12,11), 2.88));
        studenci.add(new Student("Mlynarczyk", LocalDate.of(1988,2,2), 2.12));
        studenci.add(new Student("Kowal", LocalDate.of(1988,2,2), 5.0));

        Student jacek = new Student("Jankowiak", LocalDate.of(1777,3,5), 4.5);
        studenci.add(jacek);


        Collections.sort(studenci);
        for (Student s:studenci) {
            System.out.println(s);
            System.out.println("\n");
        }

    }
}