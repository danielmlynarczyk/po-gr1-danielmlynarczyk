package pl.imiajd.Mlynarczyk;

public class Nauczyciel  extends Osoba{
    private double pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }
    public String toString() {
        String dane = "Nazwisko: " + getNazwisko() + "\nRok urodzenia: " + getRokUrodzenia() + "\nPensja: " + pensja;
        return dane;
    }
    public double getPensja(){
        return pensja;
    }


}
