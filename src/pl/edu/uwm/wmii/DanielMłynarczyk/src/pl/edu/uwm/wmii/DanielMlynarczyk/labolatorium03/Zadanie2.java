package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("zad2.txt");
        String line;
        try (Scanner scanner = new Scanner(file)) {
            line = scanner.nextLine();
        }
        System.out.println(line);
        System.out.println("Ilosc wyst�pien znaku: " + countChar(line,'a'));

    }

    public static int countChar(String str, char c){
        int ile = 0;
        for(int i = 0; i<str.length(); i++){
            if(str.charAt(i) == c){
                ile++;
            }
        }
        return ile;
    }
}