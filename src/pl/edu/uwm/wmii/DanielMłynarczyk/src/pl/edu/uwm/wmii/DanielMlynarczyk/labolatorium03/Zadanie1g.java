package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium03;

import java.util.Scanner;

public class Zadanie1g {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String napis = scanner.nextLine();
        System.out.println(napis + ": " + nice(napis));

    }
    public static String nice(String str){
        StringBuffer string = new StringBuffer("");
        int licznik = 0;
        for(int i = str.length()-1; i>=0; i--){
            if(licznik == 3) {
                string.append('\'');
                licznik = 0;
            }
            string.append(str.charAt(i));
            licznik++;
        }
        string = string.reverse();
        return string.toString();
    }
}
