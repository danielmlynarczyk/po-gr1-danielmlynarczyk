package labolatorium05;

public class RachunekBankowy {
    static double  rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double saldo){
        this.saldo = saldo;
    }
    public double obliczMiesieczneOdsetki(){
        return saldo += (saldo * rocznaStopaProcentowa) / 12;
    }
    public static void setRocznaStopaProcentowa(double stopa){
        rocznaStopaProcentowa = stopa;
    }

    public double getSaldo() {
        return saldo;
    }

}
