package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium04;

import java.util.ArrayList;

public class Zadanie2 {
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        System.out.println(merge(a,b));

    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> c = new ArrayList<>();
        if (a.size() < b.size()) {
            for (int i = 0; i < b.size(); i++) {
                c.add(b.get(i));
                if (i < a.size()) {
                    c.add(a.get(i));
                }
            }
        } else for (int i = 0; i < a.size(); i++) {
            c.add(a.get(i));
            if (i < b.size()) {
                c.add(b.get(i));
            }
        }
        return c;
    }
}