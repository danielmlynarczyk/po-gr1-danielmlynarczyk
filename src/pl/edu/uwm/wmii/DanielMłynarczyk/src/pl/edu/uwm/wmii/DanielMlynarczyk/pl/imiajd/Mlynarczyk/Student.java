package pl.imiajd.Mlynarczyk;

import java.time.LocalDate;
import java.util.Objects;

public class Student extends Osoba implements Comparable<Osoba>, Cloneable{

    private double sredniaOcen;

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String getNazwisko() {
        return super.getNazwisko();
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return Double.compare(student.sredniaOcen, sredniaOcen) == 0;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" + "[" +
                "Srednia ocen: " +getSredniaOcen() + "]";
    }
    @Override
    public int compareTo(Osoba o) {
        return super.compareTo(o);
    }

    @Override
    public void setNazwisko(String nazwisko) {
        super.setNazwisko(nazwisko);
    }

    @Override
    public LocalDate getDataUrodzenia() {
        return super.getDataUrodzenia();
    }

    @Override
    public void setDataUrodzenia(LocalDate dataUrodzenia) {
        super.setDataUrodzenia(dataUrodzenia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), sredniaOcen);
    }
}