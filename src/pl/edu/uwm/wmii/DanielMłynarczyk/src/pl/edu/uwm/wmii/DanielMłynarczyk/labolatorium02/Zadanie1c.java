package pl.edu.uwm.wmii.DanielM�ynarczyk.labolatorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1c {
    public static void main(String[] args){
        int suma = 0;
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi by� z przedzia�u [1;100]!");
            System.exit(1);
        }
        int [] tablica = new int[n];
        int max = tablica[0];
        for(int i=0; i<n; i++){
            tablica[i] = generator.nextInt(1999)-999;
            if(tablica[i]>max){
                max = tablica[i];
            }
        }
        for(int j=0; j<n; j++){
            if(max == tablica[j]){
                suma++;
            }
        }
        System.out.println(Arrays.toString(tablica));
        System.out.println(max);
        System.out.println(suma);


    }
}
