package pl.edu.uwm.wmii.DanielM�ynarczyk.labolatorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadanie2b {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi by� z przedzia�u [1;100]!");
            System.exit(1);
        }
        int[] tab = new int[n];
        generuj(tab,n,-999,1000);
        wypisz(tab);
        System.out.println("Dodatnich: "+ileDodatnich(tab));
        System.out.println("Ujemnych: "+ileUjemnych(tab));
        System.out.println("Zerowych: "+ileZerowych(tab));


    }


    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random generuj = new Random();
        for(int i=0; i<n; i++){
            tab[i] = generuj.nextInt(maxWartosc-minWartosc+1)+minWartosc;
        }
    }


    public static int ileDodatnich(int[] tab){
        int ile = 0;
        for(int i=0; i<tab.length; i++){
            if(tab[i] > 0 ){
                ile++;
            }
        }
        return ile;
    }
    public static int ileUjemnych(int[] tab){
        int ile = 0;
        for(int i=0; i<tab.length; i++){
            if(tab[i] < 0 ){
                ile++;
            }
        }
        return ile;
    }
    public static int ileZerowych(int[] tab){
        int ile = 0;
        for(int i=0; i<tab.length; i++){
            if(tab[i] == 0 ){
                ile++;
            }
        }
        return ile;
    }
    public static void wypisz(int[] tab) {
        for (int el : tab) {
            System.out.print(el + " ");
        }
        System.out.println("");
    }
}
