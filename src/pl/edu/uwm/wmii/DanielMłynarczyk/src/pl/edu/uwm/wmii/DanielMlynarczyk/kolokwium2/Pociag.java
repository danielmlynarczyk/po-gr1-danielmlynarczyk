package kolokwium2;

public class Pociag extends SrodekTransportu {
    private int dlugoscTrasy;


    public Pociag(int iloscMiejsc, int dlugoscTrasy) {
        this.iloscMiejsc = iloscMiejsc;
        this.dlugoscTrasy = dlugoscTrasy;
        obliczCene();
    }

    @Override
    void obliczCene() {
        if( this.dlugoscTrasy > 100) {
            this.cenaBiletu = this.dlugoscTrasy * 1.45;
        }
        else {
            super.obliczCene();
        }
    }

    @Override
    public String toString() {
        return "Pociag: ilosc miejsc: " + this.iloscMiejsc +
                ", dlugosc trasy: " + this.dlugoscTrasy +
                ", cena biletu: " + getCenaBiletu();
    }
}
