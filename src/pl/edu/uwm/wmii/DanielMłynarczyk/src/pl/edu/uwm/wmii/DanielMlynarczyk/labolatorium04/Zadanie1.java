package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium04;

import java.util.ArrayList;

public class Zadanie1 {
    public static void main(String[] args){
        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);

        b.add(9);
        b.add(4);
        b.add(7);
        b.add(9);
        b.add(11);
        System.out.println("Po zlaczeniu: " + append(a,b));

    }
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<>();
        c.addAll(a);
        c.addAll(b);
        return c;
    }
}
