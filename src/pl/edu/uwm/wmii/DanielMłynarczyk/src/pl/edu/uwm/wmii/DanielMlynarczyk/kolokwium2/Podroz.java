package kolokwium2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Podroz implements  IZarzadzaj, IData{
    private LocalDate dataPodrozy;
    private List<SrodekTransportu> planPodrozy = new ArrayList<>();
    private double koszt = 0;


    @Override
    public void UstawDate(LocalDate data) {
        this.dataPodrozy = data;
    }

    @Override
    public boolean SprawdzDate() {
        Date nowDate = new Date();
        int timestamp = (int) nowDate.getTime();
        /// dokonczyc
        return false;

    }

    @Override
    public void DodajAutobus(int iloscMiejsc) {
        planPodrozy.add(new Autobus(iloscMiejsc));
    }

    @Override
    public void DodajPociag(int iloscMiejsc, int dlugoscTrasy) {
        planPodrozy.add(new Pociag(iloscMiejsc, dlugoscTrasy));

    }

    @Override
    public void UsunOstatni() {
        planPodrozy.remove(planPodrozy.size() -1);

    }

    @Override
    public void Wyczysc() {
        planPodrozy.clear();

    }

    @Override
    public String toString() {
        return super.toString();
        /// dokonczyc
    }
}
