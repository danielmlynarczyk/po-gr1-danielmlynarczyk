package kolokwium2;

import java.util.ArrayList;
import java.util.Scanner;
import kolokwium2.Podroz;

public class Test {
    public static void main(String[] args) {
        Podroz podroz = new Podroz();
        Scanner scanner = new Scanner(System.in);
        System.out.println("[A] - dodaj autobus");
        System.out.println("[P] - dodaj pociag");
        System.out.println("[U] - usun ostatnia pozycje z planu podrozy");
        System.out.println("[Z] - pokaz plan podrozy");
        System.out.println("[D] - sprawdz date podrozy");
        String wybor = scanner.nextLine();

        if(wybor.equals("A")) {
            System.out.println("Podaj ilosc miejsc: ");
            int iloscMiejsc = scanner.nextInt();
            if(iloscMiejsc < 0) {
                System.err.println("Liczba miejsc jest nie poprawna!");
                System.exit(1);
            }
            else {
                podroz.DodajAutobus(iloscMiejsc);
            }
        }
        else if (wybor.equals("P")) {
            System.out.println("Podaj ilosc miejsc: ");
            int iloscMiejsc = scanner.nextInt();
            System.out.println("Podaj dlugosc trasy: ");
            int dlugoscTrasy = scanner.nextInt();
            if(iloscMiejsc < 0 || dlugoscTrasy < 0) {
                System.err.println("Liczba miejsc lub dlugosc trasy jest niepoprawna!");
                System.exit(1);
            }
            else {
                podroz.DodajPociag(iloscMiejsc, dlugoscTrasy);
            }
        }
        else if (wybor.equals("U")) {
            podroz.UsunOstatni();
        }
        else if (wybor.equals("Z")) {
            System.out.println(podroz.toString());
        }
        else if(wybor.equals("D")) {
            podroz.SprawdzDate();
        }
        else {
            System.err.println("Wybrano opcje ktora nie istnieje!");
            System.exit(1);
        }

    }
}
