package labolatorium05;

public class TestIntegerSet {
    public static void main(String[] args) throws Exception {
        IntegerSet i1 = new IntegerSet();


        i1.insertElement(2);
        i1.insertElement(5);
        System.out.println(i1.toString());
        i1.deleteElement(2);
        System.out.println(i1.toString());
        IntegerSet i2 = new IntegerSet();

        i2.insertElement(10);
        i2.insertElement(12);
        System.out.println(i2.toString());
        IntegerSet i3 =  IntegerSet.union(i1,i2);
        System.out.println(i3.toString());

    }
}
