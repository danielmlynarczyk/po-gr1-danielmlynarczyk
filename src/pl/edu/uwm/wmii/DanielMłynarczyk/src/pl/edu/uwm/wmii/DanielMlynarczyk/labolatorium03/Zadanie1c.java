package pl.edu.uwm.wmii.DanielMlynarczyk.labolatorium03;

import java.util.Scanner;

public class Zadanie1c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        System.out.println(middle(str));

    }
    public static String middle(String str){
        int middle;
        int len;
        if(str.length() % 2 == 0){
            middle = str.length()/ 2-1;
            len = 2;
        }
        else{
            middle = str.length() / 2;
            len = 1;
        }
        return str.substring(middle,middle + len);
    }
}