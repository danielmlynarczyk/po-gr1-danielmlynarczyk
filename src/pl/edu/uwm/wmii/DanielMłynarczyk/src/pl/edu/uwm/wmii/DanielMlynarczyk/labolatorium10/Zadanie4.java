package labolatorium10;

import java.util.Collections;
import java.util.LinkedList;

public class Zadanie4 {
    public static void main(String[] args) {
        LinkedList<String> lista = new LinkedList<>();
        lista.add("ala");
        lista.add("kasia");
        lista.add("ola");
        odwroc(lista);
        System.out.println(lista);

    }
    public static<T> void odwroc(LinkedList<T> lista) {
        Collections.reverse(lista);
    }
}
