package pl.edu.uwm.wmii.DanielM�ynarczyk.labolatorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1f {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi by� z przedzia�u [1;100]!");
            System.exit(1);
        }
        int [] tablica = new int[n];
        int [] tablica2 = new int[n];
        for(int i=0; i<n; i++){
            tablica[i] = generator.nextInt(1999)-999;
            if(tablica[i] > 0){
                tablica2[i] = 1;
            }
            else if(tablica[i] < 0){
                tablica2[i] = -1;
            }
        }
        System.out.println("Elementy tablicy: "+ Arrays.toString(tablica));
        System.out.println("Elementy tablicy: "+ Arrays.toString(tablica2));

    }
}
