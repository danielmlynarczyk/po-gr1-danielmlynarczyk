package pl.edu.uwm.wmii.DanielM�ynarczyk.labolatorium02;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1b {
    public static void main(String[] args){
        int dod = 0;
        int uj = 0;
        int zer = 0;
        Scanner scanner = new Scanner(System.in);
        Random generator = new Random();
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi by� z przedzia�u [1;100]!");
            System.exit(1);
        }
        int [] tablica = new int[n];
        for(int i=0; i<n; i++){
            tablica[i] = generator.nextInt(1999)-999;
            if(tablica[i]>0){
                dod++;
            }
            else if(tablica[i]<0){
                uj++;
            }
            else{
                zer++;
            }

        }
        System.out.println(Arrays.toString(tablica));
        System.out.println("Dodatnich: "+dod);
        System.out.println("Ujemnych: "+uj);
        System.out.println("Zer: "+zer);
    }
}
