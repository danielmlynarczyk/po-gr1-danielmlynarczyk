package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium00;

public class Zadanie4 {
    public static void main(String [] args){
        double op = 0.06;

        System.out.println("Saldo konta po pierwszym roku: "+(1000+op*1000));
        System.out.println("Saldo konta po drugim roku: "+(1060+op*1060));
        System.out.println("Saldo konta po trzecim roku: "+(1123.6*op+1123.6));
    }
}
