package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;

public class Zadanie2_3 {
    public static void main(String[] args){
        int dod = 0;
        int uj = 0;
        int zer = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        for(int i=0; i<n; i++){
            float liczba = scanner.nextFloat();
            if(liczba>0){
                dod++;
            }
            else if(liczba<0){
                uj++;
            }
            else{
                zer++;
            }
        }
        System.out.println("Dodatnich: "+dod);
        System.out.println("Ujemnych: "+uj);
        System.out.println("Zer: "+zer);
    }
}
