package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;

public class Zadanie2_5 {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] tablica = new int[n];

        for(int i = 0; i<n; i++)
        {
            tablica[i] = scanner.nextInt();
        }

        for(int i = 0; i<n; i++)
        {
            if(tablica[i] > 0){
                if(i+1 < n && tablica[i+1] > 0){
                    System.out.println("("+ tablica[i] + "," + tablica[i+1] + ")");
                }
            }
        }
    }
}