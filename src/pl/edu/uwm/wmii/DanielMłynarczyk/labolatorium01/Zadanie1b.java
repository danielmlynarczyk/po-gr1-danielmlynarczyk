package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;

public class Zadanie1b {
    public static void main(String[] args){
        float iloczyn = 1;
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);
        float n = scanner.nextFloat();

        for(int i = 0; i<n; i++){

            float liczba = scanner.nextFloat();
            iloczyn*=liczba;
        }
        System.out.println(iloczyn);
    }
}
