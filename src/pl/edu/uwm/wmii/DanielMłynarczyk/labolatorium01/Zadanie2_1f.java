package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;

public class Zadanie2_1f {
    public static void main(String[] args){
        int pom = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        for(int i=1; i<=n; i++){
            int liczba = scanner.nextInt();
            if(i % 2 == 1 && liczba % 2 == 0){
                pom++;
            }
        }
        System.out.println(pom);
    }
}
