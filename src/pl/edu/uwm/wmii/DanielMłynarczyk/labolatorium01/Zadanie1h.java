package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;

public class Zadanie1h {
    public static void main(String[] args) {

        float suma = 0;

        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);
        float n = scanner.nextFloat();

        for(int i=0; i<n; i++)
        {
            float liczba = scanner.nextFloat();
            suma += Math.pow(-1, i+1) * liczba;
        }
        System.out.println(suma);
    }
}
