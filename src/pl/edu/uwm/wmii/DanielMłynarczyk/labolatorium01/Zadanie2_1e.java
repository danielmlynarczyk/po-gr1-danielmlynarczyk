package pl.edu.uwm.wmii.DanielM�ynarczyk.labolatorium01;

import java.util.Scanner;

public class Zadanie2_1e {

    private static int silnia(int i)
    {
        if (i<1)
            return 1;
        else
            return i*silnia(i-1);
    }

    public static void main(String[] args) {
        int pom = 0;
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for(int i=0; i<n; i++)
        {
            int liczba = scanner.nextInt();
            if(Math.pow(2, liczba) < liczba && liczba < silnia(liczba)){
                pom++;
            }
        }
        System.out.print(pom);
    }
}