package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;

import java.lang.Math;

public class Zadanie1d {
    public static void main(String[] args){
        float suma = 0;
        float pom;
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);
        float n = scanner.nextFloat();

        for(int i=0; i<n; i++){
            float liczba = scanner.nextFloat();
            pom = Math.abs(liczba);
            suma+=Math.sqrt(pom);
        }
        System.out.println(suma);

    }
}
