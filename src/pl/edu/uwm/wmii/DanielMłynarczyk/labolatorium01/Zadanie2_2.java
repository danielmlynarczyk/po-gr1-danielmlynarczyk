package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;

public class Zadanie2_2 {
    public static void main(String[] args){
        float suma =0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        for(int i=0; i<n; i++){
            float liczba = scanner.nextFloat();
            if(liczba>0){
                suma = suma+liczba*2;
            }
        }
        System.out.println(suma);
    }
}
