package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;


import java.util.Scanner;

public class Zadanie2_1c {
    public static void main(String[] args) {
        int pom = 0;
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for(int i=0; i<n; i++)
        {
            int liczba = scanner.nextInt();
            if(liczba % 2 == 0 && Math.sqrt(liczba) == (int)Math.sqrt(liczba)){
                pom++;
            }

        }
        System.out.println(pom);
    }
}