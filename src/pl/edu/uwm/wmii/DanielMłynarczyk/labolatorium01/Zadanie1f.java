package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;
import java.lang.Math;
public class Zadanie1f {
    public static void main(String [] args){

        float suma = 0;

        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);
        float n = scanner.nextFloat();

        for(int i=0; i <n; i++){
            float a = scanner.nextFloat();
            suma+=Math.pow(a,2);
        }
        System.out.println(suma);

    }
}
