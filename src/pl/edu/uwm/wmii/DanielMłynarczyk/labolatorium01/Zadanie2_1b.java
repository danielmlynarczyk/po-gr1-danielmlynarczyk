package pl.edu.uwm.wmii.DanielMłynarczyk.labolatorium01;

import java.util.Scanner;

public class Zadanie2_1b {
    public static void main(String[] args){
        int pom=0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        for(int i =0; i<n; i++){
            int liczba = scanner.nextInt();
            if((liczba % 3 == 0) && (liczba % 5 != 0)){
                pom++;
            }
        }
        System.out.println("Liczb które są podzielne przez 3 i niepodzielne przez 5 jest: "+pom );

    }
}
